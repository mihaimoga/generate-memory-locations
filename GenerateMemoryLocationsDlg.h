
// GenerateMemoryLocationsDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

// CGenerateMemoryLocationsDlg dialog
class CGenerateMemoryLocationsDlg : public CDialogEx
{
// Construction
public:
	CGenerateMemoryLocationsDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GENERATEMEMORYLOCATIONS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;
	CEdit m_pPageFile;
	CEdit m_pStringFile;
	CEdit m_pOutputFile;
	CButton m_pStart;
	CProgressCtrl m_pProgress;
	CString m_strPageFile;
	CString m_strStringFile;
	CString m_strOutputFile;
	std::map<std::string, std::string> pLanguage;
	std::map<int, std::string> pMemoryLocations;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedConvert();
};
