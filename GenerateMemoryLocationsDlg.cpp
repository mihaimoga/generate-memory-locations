
// GenerateMemoryLocationsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GenerateMemoryLocations.h"
#include "GenerateMemoryLocationsDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CGenerateMemoryLocationsDlg dialog

CGenerateMemoryLocationsDlg::CGenerateMemoryLocationsDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_GENERATEMEMORYLOCATIONS_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGenerateMemoryLocationsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_pPageFile);
	DDX_Control(pDX, IDC_EDIT2, m_pStringFile);
	DDX_Control(pDX, IDC_EDIT3, m_pOutputFile);
	DDX_Control(pDX, IDC_CONVERT, m_pStart);
	DDX_Control(pDX, IDC_PROGRESS1, m_pProgress);
}

BEGIN_MESSAGE_MAP(CGenerateMemoryLocationsDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CGenerateMemoryLocationsDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CGenerateMemoryLocationsDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CGenerateMemoryLocationsDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_CONVERT, &CGenerateMemoryLocationsDlg::OnBnClickedConvert)
END_MESSAGE_MAP()


// CGenerateMemoryLocationsDlg message handlers

BOOL CGenerateMemoryLocationsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_strPageFile = AfxGetApp()->GetProfileString(_T("GenerateMemoryLocations"), _T("PageFile"), _T(""));
    m_pPageFile.SetWindowText(m_strPageFile);
    m_strStringFile = AfxGetApp()->GetProfileString(_T("GenerateMemoryLocations"), _T("StringFile"), _T(""));
    m_pStringFile.SetWindowText(m_strStringFile);
    m_strOutputFile = AfxGetApp()->GetProfileString(_T("GenerateMemoryLocations"), _T("OutputFile"), _T(""));
	m_pOutputFile.SetWindowText(m_strOutputFile);
	m_pStart.EnableWindow(!m_strPageFile.IsEmpty() && !m_strStringFile.IsEmpty() && !m_strOutputFile.IsEmpty());

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGenerateMemoryLocationsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGenerateMemoryLocationsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGenerateMemoryLocationsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CGenerateMemoryLocationsDlg::OnBnClickedButton1()
{
	DWORD dwFlags = OFN_DONTADDTORECENT | OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_LONGNAMES;
	LPCTSTR lpszFilter = _T("XAML files (*.xaml)|*.xaml\0");
	CFileDialog pFileDialog(TRUE, NULL, NULL, dwFlags, lpszFilter, this);
	if (pFileDialog.DoModal() == IDOK)
	{
		m_strPageFile = pFileDialog.GetPathName();
		m_pPageFile.SetWindowText(pFileDialog.GetPathName());
		m_pStart.EnableWindow(!m_strPageFile.IsEmpty() && !m_strStringFile.IsEmpty() && !m_strOutputFile.IsEmpty());
		AfxGetApp()->WriteProfileString(_T("GenerateMemoryLocations"), _T("PageFile"), m_strPageFile);
	}
}

void CGenerateMemoryLocationsDlg::OnBnClickedButton2()
{
	DWORD dwFlags = OFN_DONTADDTORECENT | OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_LONGNAMES;
	LPCTSTR lpszFilter = _T("XAML files (*.xaml)|*.xaml\0");
	CFileDialog pFileDialog(TRUE, NULL, NULL, dwFlags, lpszFilter, this);
	if (pFileDialog.DoModal() == IDOK)
	{
		m_strStringFile = pFileDialog.GetPathName();
		m_pStringFile.SetWindowText(pFileDialog.GetPathName());
		m_pStart.EnableWindow(!m_strPageFile.IsEmpty() && !m_strStringFile.IsEmpty() && !m_strOutputFile.IsEmpty());
		AfxGetApp()->WriteProfileString(_T("GenerateMemoryLocations"), _T("StringFile"), m_strStringFile);
	}
}

void CGenerateMemoryLocationsDlg::OnBnClickedButton3()
{
	DWORD dwFlags = OFN_DONTADDTORECENT | OFN_ENABLESIZING | OFN_EXPLORER | OFN_HIDEREADONLY | OFN_LONGNAMES;
	LPCTSTR lpszFilter = _T("XML files (*.xml)|*.xml\0");
	CFileDialog pFileDialog(FALSE, NULL, NULL, dwFlags, lpszFilter, this);
	if (pFileDialog.DoModal() == IDOK)
	{
		m_strOutputFile = pFileDialog.GetPathName();
		m_pOutputFile.SetWindowText(pFileDialog.GetPathName());
		m_pStart.EnableWindow(!m_strPageFile.IsEmpty() && !m_strStringFile.IsEmpty() && !m_strOutputFile.IsEmpty());
		AfxGetApp()->WriteProfileString(_T("GenerateMemoryLocations"), _T("OutputFile"), m_strOutputFile);
	}
}

void CGenerateMemoryLocationsDlg::OnBnClickedConvert()
{
	char* lpszInput = NULL;
	char* lpszOutput = NULL;
	char* lpszBackup = NULL;
	char message[0x1000] = { 0, };
	CString Buffer;
	m_pStart.EnableWindow(FALSE);
	try
	{
		CFile pInputFile(m_strPageFile, CFile::modeRead | CFile::typeBinary);
		const UINT dwInput = (UINT)pInputFile.GetLength();
		if (dwInput > 0)
		{
			lpszInput = new char[dwInput + 1];
			lpszBackup = new char[dwInput + 1];
			memset(lpszInput, 0, dwInput + 1);
			memset(lpszBackup, 0, dwInput + 1);
			if (dwInput == pInputFile.Read(lpszInput, dwInput))
			{
				memcpy(lpszBackup, lpszInput, dwInput);
				CFile pOutputFile(m_strStringFile, CFile::modeRead | CFile::typeBinary);
				const UINT dwOutput = (UINT)pOutputFile.GetLength();
				if (dwOutput > 0)
				{
					lpszOutput = new char[dwOutput + 1];
					memset(lpszOutput, 0, dwOutput + 1);
					if (dwOutput == pOutputFile.Read(lpszOutput, dwOutput))
					{
						OutputDebugString(_T("Starting parsing language...\n"));
						char* searchKey = strstr(lpszOutput, "x:Key");
						while (searchKey != NULL)
						{
							searchKey += strlen("x:Key");
							char* startKey = strchr(searchKey, '\"');
							if (startKey != NULL)
							{
								startKey++;
								char* finishKey = strchr(startKey, '\"');
								if (finishKey != NULL)
								{
									finishKey[0] = 0;
									finishKey++;
									searchKey = finishKey;

									char* startValue = strchr(searchKey, '>');
									if (startValue != NULL)
									{
										startValue++;
										char* finishValue = strchr(searchKey, '<');
										if (finishValue != NULL)
										{
											finishValue[0] = 0;
											finishValue++;
											searchKey = finishValue;

											sprintf(message, "%s=%s\n", startKey, startValue);
											OutputDebugStringA(message);
											pLanguage[startKey] = startValue;
										}
									}
								}
							}
							searchKey = strstr(searchKey, "x:Key");
						}

						OutputDebugString(_T("Starting parsing controls...\n"));
						searchKey = strstr(lpszInput, "TextBlock");
						while (searchKey != NULL)
						{
							searchKey += strlen("TextBlock");
							char* finishKey = strchr(searchKey, '/');
							if ((finishKey != NULL) && finishKey[1] == '>')
							{
								finishKey[0] = 0;
								finishKey++;

								char* startText = strstr(searchKey, "Text=");
								if (startText != NULL)
								{
									startText += strlen("Text=");
									char* startID = strchr(startText, ' ');
									char* endID = strchr(startText, '}');
									if ((startID != NULL) && (endID != NULL))
									{
										startID++;
										endID[0] = 0;
										endID++;

										char* toolTip = strstr(endID, "ToolTip=");
										if (toolTip != NULL)
										{
											char* startQuote = strchr(toolTip, '\"');
											if (startQuote != NULL)
											{
												startQuote++;
												char* endQuote = strchr(startQuote, '\"');
												if (endQuote != NULL)
												{
													endQuote[0] = 0;
													endQuote++;
													searchKey = endQuote;

													//sprintf(message, "%s=%s\n", startID, startQuote);
													//OutputDebugStringA(message);
													if (strchr(startQuote, ',') != NULL)
													{
														char* rootLocation = strtok(startQuote, ",");
														if (rootLocation != NULL)
														{
															char* gap = strtok(NULL, ",");
															if (gap != NULL)
															{
																char* controler = strtok(NULL, ",");
																if (controler != NULL)
																{
																	char* index = strtok(NULL, ",");
																	if (index != NULL)
																	{
																		if (strtok(NULL, ",") != NULL)
																		{
																			// ignore
																		}
																		else
																		{
																			if (strcmp(controler, "c_card") == 0)
																			{
																				for (int card = 0; card < 20; card++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * card + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			if (strcmp(controler, "c_pos_bin") == 0)
																			{
																				for (int card = 0; card < 40; card++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * card + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_host_a") == 0)
																			{
																				for (int host = 0; host < 10; host++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * host + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_emv") == 0)
																			{
																				for (int emv = 0; emv < 10; emv++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * emv + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "cDRL_id") == 0)
																			{
																				for (int DRL = 0; DRL < 4; DRL++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * DRL + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_ctls_tip") == 0)
																			{
																				for (int ctls_tip = 0; ctls_tip < 10; ctls_tip++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * ctls_tip + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_ctls_aid") == 0)
																			{
																				for (int ctls_aid = 0; ctls_aid < 10; ctls_aid++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * ctls_aid + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
													else
													{
														if (strchr(startQuote, ' ') != NULL)
														{
															// ignore
														}
														else
														{
															if (pMemoryLocations[atoi(startQuote)].empty())
															{
																pMemoryLocations[atoi(startQuote)] = pLanguage[startID];
																sprintf(message, "%d=%s\n", atoi(startQuote), pMemoryLocations[atoi(startQuote)].c_str());
																OutputDebugStringA(message);
															}
														}
													}
												}
											}
										}
									}
								}
								searchKey = finishKey;
							}
							searchKey = strstr(searchKey, "TextBlock");
						}

						searchKey = strstr(lpszBackup, "CheckBox");
						while (searchKey != NULL)
						{
							searchKey += strlen("CheckBox");
							char* finishKey = strchr(searchKey, '/');
							if ((finishKey != NULL) && finishKey[1] == '>')
							{
								finishKey[0] = 0;
								finishKey++;

								char* startText = strstr(searchKey, "Content=");
								if (startText != NULL)
								{
									startText += strlen("Content=");
									char* startID = strchr(startText, ' ');
									char* endID = strchr(startText, '}');
									if ((startID != NULL) && (endID != NULL))
									{
										startID++;
										endID[0] = 0;
										endID++;

										char* toolTip = strstr(endID, "ToolTip=");
										if (toolTip != NULL)
										{
											char* startQuote = strchr(toolTip, '\"');
											if (startQuote != NULL)
											{
												startQuote++;
												char* endQuote = strchr(startQuote, '\"');
												if (endQuote != NULL)
												{
													endQuote[0] = 0;
													endQuote++;
													searchKey = endQuote;

													//sprintf(message, "%s=%s\n", startID, startQuote);
													//OutputDebugStringA(message);
													if (strchr(startQuote, ',') != NULL)
													{
														char* rootLocation = strtok(startQuote, ",");
														if (rootLocation != NULL)
														{
															char* gap = strtok(NULL, ",");
															if (gap != NULL)
															{
																char* controler = strtok(NULL, ",");
																if (controler != NULL)
																{
																	char* index = strtok(NULL, ",");
																	if (index != NULL)
																	{
																		if (strtok(NULL, ",") != NULL)
																		{
																			// ignore
																		}
																		else
																		{
																			if (strcmp(controler, "c_card") == 0)
																			{
																				for (int card = 0; card < 20; card++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * card + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			if (strcmp(controler, "c_pos_bin") == 0)
																			{
																				for (int card = 0; card < 40; card++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * card + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_host_a") == 0)
																			{
																				for (int host = 0; host < 10; host++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * host + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_emv") == 0)
																			{
																				for (int emv = 0; emv < 10; emv++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * emv + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "cDRL_id") == 0)
																			{
																				for (int DRL = 0; DRL < 4; DRL++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * DRL + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_ctls_tip") == 0)
																			{
																				for (int ctls_tip = 0; ctls_tip < 10; ctls_tip++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * ctls_tip + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																			else if (strcmp(controler, "c_ctls_aid") == 0)
																			{
																				for (int ctls_aid = 0; ctls_aid < 10; ctls_aid++)
																				{
																					int no = atoi(rootLocation) + atoi(gap) * ctls_aid + atoi(index);
																					pMemoryLocations[no] = pLanguage[startID];
																					sprintf(message, "%d=%s\n", no, pMemoryLocations[no].c_str());
																					OutputDebugStringA(message);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
													else
													{
														if (strchr(startQuote, ' ') != NULL)
														{
															// ignore
														}
														else
														{
															if (pMemoryLocations[atoi(startQuote)].empty())
															{
																pMemoryLocations[atoi(startQuote)] = pLanguage[startID];
																sprintf(message, "%d=%s\n", atoi(startQuote), pMemoryLocations[atoi(startQuote)].c_str());
																OutputDebugStringA(message);
															}
														}
													}
												}
											}
										}
									}
								}
								searchKey = finishKey;
							}
							searchKey = strstr(searchKey, "CheckBox");
						}
					}
				}
				pOutputFile.Close();
			}
		}
		pInputFile.Close();

		CStdioFile pXmlFile(m_strOutputFile, CFile::modeCreate | CFile::modeWrite | CFile::typeText);
		pXmlFile.WriteString(_T("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"));
		pXmlFile.WriteString(_T("<MemoryLocations>\n"));
		for (int index = 0; index < 20000; index++)
		{
			if (!pMemoryLocations[index].empty())
			{
				Buffer.Format(_T("<Parameter ID=\"%d\">%s</Parameter>\n"), index, CString(pMemoryLocations[index].c_str()));
				pXmlFile.WriteString(Buffer);
			}
		}
		pXmlFile.WriteString(_T("</MemoryLocations>\n"));
		pXmlFile.Close();
	}
	catch (CFileException * pFileException)
	{
		TCHAR lpszError[0x100] = { 0 };
		pFileException->GetErrorMessage(lpszError, sizeof(lpszError) / sizeof(TCHAR));
		pFileException->Delete();
		OutputDebugString(lpszError);
	}

	if (lpszInput != NULL)
	{
		delete lpszInput;
		lpszInput = NULL;
	}
	if (lpszOutput != NULL)
	{
		delete lpszOutput;
		lpszOutput = NULL;
	}
	if (lpszBackup != NULL)
	{
		delete lpszBackup;
		lpszBackup = NULL;
	}
	AfxMessageBox(_T("The quick brown fox jumps over the lazy dog"));
	m_pStart.EnableWindow(TRUE);
}
